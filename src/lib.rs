pub mod protocol;
pub mod parser;


extern crate base64;
extern crate bytes;
#[macro_use]
extern crate clap;
extern crate futures;
extern crate hmac;
extern crate rand;
extern crate sha2;
extern crate time;
extern crate tokio;
extern crate tokio_io;
extern crate tokio_timer;
extern crate tokio_executor;
extern crate tokio_codec;
extern crate websocket;

#[macro_use]
extern crate hyper;

#[macro_use]
extern crate error_chain;

extern crate serde;
#[macro_use]
extern crate serde_derive;
extern crate serde_yaml;
pub(crate) mod errors {
    error_chain! {
        foreign_links {
            IoError(::std::io::Error);
            AddrParseError(::std::net::AddrParseError);
            ParseIntError(::std::num::ParseIntError);
            Utf8Error(::std::str::Utf8Error);
            WebSocketError(::websocket::WebSocketError);
            ParseError(::websocket::client::builder::ParseError);
        }
    }
}
